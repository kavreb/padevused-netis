﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace WebApplication3.Controllers
{
    public class HomeController : Controller
    {
        private InimesedPadevusedEntities db = new InimesedPadevusedEntities();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            if (Request.IsAuthenticated)
            {
                ViewBag.Kasutajanimi = db.Kasutaja.Where(user => user.Email == User.Identity.Name).SingleOrDefault()?.Nimi ?? "Pole meie kasutaja";
            }
            else
            {
                ViewBag.Kasutajanimi = "";
            }
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Nimekiri()
        {
            return View();
        }

        public ActionResult Contact()
        {
            if (Request.IsAuthenticated)
            {
                ViewBag.Kasutajanimi = db.Kasutaja.Where(user => user.Email == User.Identity.Name).SingleOrDefault()?.Nimi ?? "Puudub andmebaasist";
            }
            else
            {
                ViewBag.Kasutajanimi = "";
            }

            ViewBag.TheMessage = "Saada meile teade!";
            return View();
        }

        [HttpPost]
        public ActionResult Contact(string message)
        {            
             //TODO: send message to HQ
            ViewBag.TheMessage = "Saime su teate kätte!";

            return View();
        }


    }
}