﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;
using PagedList;

namespace WebApplication3 // Tegime partial classi et saaks oskust lisada
{
    partial class Kasutaja
    {
        public string LisaOskus { get; set; }
        public string LisaTase { get; set; }

    }
}

namespace WebApplication3.Controllers
{
    public class KasutajaController : Controller
    {
        private InimesedPadevusedEntities db = new InimesedPadevusedEntities();
        // GET: Kasutaja
        public ActionResult Index()
        {
            var kasutaja = db.Kasutaja.Include(k => k.Osakond);
            return View(kasutaja.ToList());
        }

        public ActionResult Nimekiri(string sortOrder, string searchWord, string currentFilter, int? page)
        {
            var kasutaja = from k in db.Kasutaja select k;

            var pageNumber = page ?? 1;
            int pageSize = 10;

            ViewBag.CurrentSort = sortOrder;

            if (searchWord != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchWord = currentFilter;
            }

            ViewBag.CurrentFilter = searchWord;

            if (!string.IsNullOrEmpty(searchWord))
            {
                kasutaja = kasutaja.Where(s => s.Nimi.Contains(searchWord.Trim()) 
                                            || s.Email.Contains(searchWord.Trim()) 
                                            || s.Osakond.Nimetus.Contains(searchWord.Trim())
                                            || s.KasutajaRoll.Select(k => k.Roll.Nimetus).Contains(searchWord.Trim())
                                            || s.KasutajaOskus.Select(k => k.Oskus.Nimetus).Contains(searchWord.Trim())
                                            || s.KasutajaOskus.Select(k => k.Tase.ToString()).Contains(searchWord.ToString())
                                            );
            }

            switch (sortOrder)
            {
                case "name_desc":
                    ViewBag.Suund = "name_desc";
                    kasutaja = kasutaja.OrderByDescending(x => x.Nimi).ThenBy(s => s.Id);
                    break;

                case "name_asc":
                    ViewBag.Suund = "name_asc";
                    kasutaja = kasutaja.OrderBy(x => x.Nimi).ThenBy(s => s.Id);
                    break;

                case "email_desc":
                    ViewBag.Email = "email_desc";
                    kasutaja = kasutaja.OrderBy(x => x.Email);
                    break;

                case "email_asc":
                    ViewBag.Email = "email_asc";
                    kasutaja = kasutaja.OrderByDescending(x => x.Email);
                    break;

                case "osak_desc":
                    ViewBag.Osak = "osak_desc";
                    kasutaja = kasutaja.OrderBy(x => x.Osakond.Nimetus);
                    break;

                case "osak_asc":
                    ViewBag.Osak = "osak_asc";
                    kasutaja = kasutaja.OrderByDescending(x => x.Osakond.Nimetus);
                    break;

                case "oskus_desc":
                    ViewBag.Oskus = "oskus_desc";
                    kasutaja = kasutaja.OrderBy(x => x.KasutajaOskus.Select(y => y.Oskus.Nimetus).SingleOrDefault());
                    break;

                case "oskus_asc":
                    ViewBag.Oskus = "oskus_asc";
                    kasutaja = kasutaja.OrderByDescending(x => x.KasutajaOskus.Select(y => y.Oskus.Nimetus).SingleOrDefault());
                    break;

                case "inaktiv":
                    ViewBag.Akt = "inaktiv";
                    kasutaja = kasutaja.OrderBy(x => x.Aktiivsus);
                    break;

                case "aktiv":
                    ViewBag.Akt = "aktiv";
                    kasutaja = kasutaja.OrderByDescending(x => x.Aktiivsus);
                    break;
                    
                default:
                    kasutaja = kasutaja.OrderBy(x => x.Nimi);
                    break;
            }

            var onePageOfNames = kasutaja.ToPagedList(pageNumber, pageSize);
            ViewBag.OnePageOfNames= onePageOfNames;

            return View(kasutaja.ToPagedList(pageNumber, pageSize));
            
        }

        // GET: Kasutaja/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            ViewBag.Kasutaja = kasutaja.Email;

            return View(kasutaja);
        }

        // GET: Kasutaja/Create
        public ActionResult Create()
        {
            ViewBag.OsakondID = new SelectList(db.Osakond, "Id", "Nimetus");
            return View();
        }

        // POST: Kasutaja/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Email,Nimi,OsakondID, Aktiivsus")] Kasutaja kasutaja)
        {
            if (ModelState.IsValid)
            {
                db.Kasutaja.Add(kasutaja);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OsakondID = new SelectList(db.Osakond, "Id", "Nimetus", kasutaja.OsakondID);
            return View(kasutaja);
        }

        // GET: Kasutaja/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            ViewBag.Kasutaja = kasutaja.Email;
            ViewBag.OsakondID = new SelectList(db.Osakond, "Id", "Nimetus", kasutaja.OsakondID);

            kasutaja.LisaOskus = "";
            kasutaja.LisaTase = "";
            return View(kasutaja);
        }

        // POST: Kasutaja/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //TODO SIIN PEAB UURIMA KUIDAS KORDA TEHA!!!
        //[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Email,Nimi,OsakondID, Aktiivsus, LisaOskus")] Kasutaja kasutaja)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutaja).State = EntityState.Modified;
                db.SaveChanges();
                //return RedirectToAction("Index");
                string lisaOskus = (kasutaja.LisaOskus + "").Trim().ToLower();
                if (lisaOskus == "") return RedirectToAction("Index");
                Oskus oskused = db.Oskus.Where(x => x.Nimetus.ToLower() == lisaOskus).Take(1).SingleOrDefault();
                if (oskused == null)
                {
                    oskused = new Oskus { Nimetus = kasutaja.LisaOskus };
                    db.SaveChanges();
                }
                kasutaja.KasutajaOskus.Add(new KasutajaOskus { Oskus = oskused });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Kasutaja = kasutaja.Email;
            ViewBag.OsakondID = new SelectList(db.Osakond, "Id", "Nimetus", kasutaja.OsakondID);
            return View(kasutaja);
        }

        // GET: Kasutaja/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            if (kasutaja == null)
            {
                return HttpNotFound();
            }
            return View(kasutaja);
        }

        // POST: Kasutaja/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kasutaja kasutaja = db.Kasutaja.Find(id);
            db.Kasutaja.Remove(kasutaja);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
