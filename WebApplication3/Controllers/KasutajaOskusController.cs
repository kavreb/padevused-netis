﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;
using PagedList;
using System.Data.Entity.Core.Objects;

namespace WebApplication3.Controllers
{
    
    public class KasutajaOskusController : Controller
    {
        private InimesedPadevusedEntities db = new InimesedPadevusedEntities();

        // GET: KasutajaOskus
        public ActionResult Index(string sortOrder, string currentFilter, string searchWord, int? page)
        {
            //var kasutajaOskus = kasutajaOskusAlg.ToList();

            var pageNumber = page ?? 1;
            int pageSize = 10;

            var kasutajaOskus = from k in db.KasutajaOskus select k;

            ViewBag.CurrentSort = sortOrder;

            if (searchWord != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchWord = currentFilter;
            }

            ViewBag.CurrentFilter = searchWord;

            if (!string.IsNullOrEmpty(searchWord))
            {
                kasutajaOskus = kasutajaOskus.Where(s => s.Kasutaja.Nimi.Contains(searchWord.Trim())
                                                        || s.Oskus.Nimetus.Contains(searchWord.Trim())
                                                        || s.Tase.ToString().Contains(searchWord.ToString().Trim())
                                                        );
            }

            switch (sortOrder)
            {
                case "date_desc":
                    ViewBag.Date = "date_desc";
                    kasutajaOskus = kasutajaOskus.OrderByDescending(x => x.Kuupäev);
                    break;

                case "date_asc":
                    ViewBag.Date = "date_asc";
                    kasutajaOskus = kasutajaOskus.OrderBy(x => x.Kuupäev);
                    break;

                case "name_desc":
                    ViewBag.Name = "name_desc";
                    kasutajaOskus = kasutajaOskus.OrderByDescending(x => x.Kasutaja.Nimi).ThenBy(s => s.KasutajaId);
                    break;

                case "name_asc":
                    ViewBag.Name = "name_asc";
                    kasutajaOskus = kasutajaOskus.OrderBy(x => x.Kasutaja.Nimi).ThenBy(s => s.KasutajaId);
                    break;

                case "oskus_desc":
                    ViewBag.Name = "oskus_desc";
                    kasutajaOskus = kasutajaOskus.OrderByDescending(x => x.Oskus.Nimetus).ThenBy(s => s.Tase);
                    break;

                case "oskus_asc":
                    ViewBag.Name = "oskus_asc";
                    kasutajaOskus = kasutajaOskus.OrderBy(x => x.Oskus.Nimetus).ThenBy(s => s.Tase);
                    break;

                case "tase_desc":
                    ViewBag.Name = "tase_desc";
                    kasutajaOskus = kasutajaOskus.OrderByDescending(x => x.Tase);
                    break;

                case "tase_asc":
                    ViewBag.Name = "tase_asc";
                    kasutajaOskus = kasutajaOskus.OrderBy(x => x.Tase);
                    break;

                default:
                    ViewBag.Name = "name_asc";
                    kasutajaOskus = kasutajaOskus.OrderBy(x => x.Kasutaja.Nimi);
                    break;
            }

            var onePageOfSkills = kasutajaOskus.ToPagedList(pageNumber, pageSize);
            ViewBag.OnePageOfSkills = onePageOfSkills;

            return View(kasutajaOskus.ToPagedList(pageNumber, pageSize));
        }

        [HttpPost]
        public ActionResult Upload (HttpPostedFileBase postedFile,int? id)
        {
            KasutajaOskus kasutajaOskus = db.KasutajaOskus.Find(id);
            if (postedFile == null)
            {
                ViewBag.Message = "Please sellect file!";
                return View();
            }
            else if (postedFile != null)
            {
                string path = Server.MapPath("~/Uploads/");
 
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                postedFile.SaveAs(path + Path.GetFileName(postedFile.FileName));
                ViewBag.Message9 = "File uploaded successfully!";

            }
            return View();
        }

        // GET: KasutajaOskus/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaOskus kasutajaOskus = db.KasutajaOskus.Find(id);
            if (kasutajaOskus == null)
            {
                return HttpNotFound();
            }
            ViewBag.Kasutaja = kasutajaOskus.Kasutaja.Email;

            return View(kasutajaOskus);
        }
        
        // GET: KasutajaOskus/Create
        public ActionResult Create()
        {
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email");
            ViewBag.OskusId = new SelectList(db.Oskus, "Id", "Nimetus");
            return View();
        }

        // POST: KasutajaOskus/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,KasutajaId,OskusId,Tase,Kuupäev")] KasutajaOskus kasutajaOskus)
        {
            if (ModelState.IsValid)
            {
                db.KasutajaOskus.Add(kasutajaOskus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kasutajaOskus.KasutajaId);
            ViewBag.OskusId = new SelectList(db.Oskus, "Id", "Nimetus", kasutajaOskus.OskusId);
            return View(kasutajaOskus);
        }

        // GET: KasutajaOskus/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaOskus kasutajaOskus = db.KasutajaOskus.Find(id);
            if (kasutajaOskus == null)
            {
                return HttpNotFound();
            }

            ViewBag.Kasutaja = db.KasutajaOskus.Find(id).Kasutaja.Email;
                
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kasutajaOskus.KasutajaId);
            ViewBag.OskusId = new SelectList(db.Oskus, "Id", "Nimetus", kasutajaOskus.OskusId);

            return View(kasutajaOskus);
           
        }

        // POST: KasutajaOskus/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,KasutajaId,OskusId,Tase,Kuupäev,LisaOskus")] KasutajaOskus kasutajaOskus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutajaOskus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Kasutaja = kasutajaOskus.Kasutaja.Email;

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kasutajaOskus.KasutajaId);
            ViewBag.OskusId = new SelectList(db.Oskus, "Id", "Nimetus", kasutajaOskus.OskusId);
            return View(kasutajaOskus);
        }

        // GET: KasutajaOskus/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaOskus kasutajaOskus = db.KasutajaOskus.Find(id);
            if (kasutajaOskus == null)
            {
                return HttpNotFound();
            }
            ViewBag.Kasutaja = kasutajaOskus.Kasutaja.Email;

            return View(kasutajaOskus);
        }

        // POST: KasutajaOskus/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KasutajaOskus kasutajaOskus = db.KasutajaOskus.Find(id);
            db.KasutajaOskus.Remove(kasutajaOskus);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
