﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;


namespace WebApplication3.Controllers
{


    public class OsakondController : Controller
    {
        private InimesedPadevusedEntities db = new InimesedPadevusedEntities();

        // GET: Osakond
        public ActionResult Index(string sortOrder)
        {
            var depDb = db.Osakond;
            //from k in db.Osakond select k;

            ViewBag.Mingi = db.Kasutaja;
            

            if (sortOrder != null)
            {
                switch (sortOrder)
                {
                    case "osak_desc":
                        ViewBag.Osak = "osak_desc";
                        return View(depDb.ToList().OrderByDescending(x => x.Nimetus));

                    case "osak_asc":
                        ViewBag.Osak = "osak_asc";
                        return View(depDb.ToList().OrderBy(x => x.Nimetus));

                    case "osakManager_asc":
                        ViewBag.OsakMan = "osakManager_asc";
                        var departmentManagerName = depDb.ToList().OrderBy(x => Kasutaja.ById(x.ManagerId)?.Nimi);
                        return View(departmentManagerName);

                    case "osakManager_desc":
                        ViewBag.OsakMan = "osakManager_desc";
                        departmentManagerName = depDb.ToList().OrderByDescending(x => Kasutaja.ById(x.ManagerId)?.Nimi);
                        return View(departmentManagerName.ToList());

                    default:
                        return View(depDb.ToList().OrderBy(x => x.Nimetus));
                }
            }
            else
            {
                ViewBag.Osak = "osak_asc";
                return View(depDb.ToList().OrderBy(x => x.Nimetus));
            }
        }

        // GET: Osakond/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Osakond osakond = db.Osakond.Find(id);
            if (osakond == null)
            {
                return HttpNotFound();
            }
            return View(osakond);
        }

        // GET: Osakond/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Osakond/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nimetus")] Osakond osakond)
        {
            if (ModelState.IsValid)
            {
                db.Osakond.Add(osakond);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(osakond);
        }

        // GET: Osakond/CREATEMANAGER
        public ActionResult CreateManager()
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}

            //Osakond osakond = db.Osakond.Find();

            //if (osakond == null)
            //{
            //    return HttpNotFound();
            //}

            ViewBag.Nimetus = new SelectList(db.Osakond, "Id", "Nimetus");
            ViewBag.ManagerId = new SelectList(db.Kasutaja.Where(x => x.KasutajaRoll.Select(y => y.RollId).Contains(3)).ToList(), "Id", "Nimi");
            return View();
        }

        // POST: Osakond/CREATEMANAGER
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateManager([Bind(Include = "Id,Nimetus,ManagerId")] Osakond osakond)
        {
            

            if (ModelState.IsValid)
            {
           
                db.Entry(osakond).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Nimetus = new SelectList(db.Osakond, "Id", "Nimetus", osakond.Nimetus);
            ViewBag.ManagerId = new SelectList(db.Kasutaja.Where(x => x.KasutajaRoll.Select(y => y.RollId).Contains(3)).ToList(), "Id", "Nimi", osakond.ManagerId);
            
            return View();
        }

        // GET: Osakond/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Osakond osakond = db.Osakond.Find(id);

            ViewBag.Nimetus = new SelectList(db.Osakond, "Id", "Nimetus", osakond.Nimetus);
            ViewBag.ManagerId = new SelectList(db.Kasutaja.Where(x => x.KasutajaRoll.Select(y => y.RollId).Contains(3)).ToList(), "Id", "Nimi", osakond.ManagerId);

            if (osakond == null)
            {
                return HttpNotFound();
            }
            return View(osakond);
        }

        // POST: Osakond/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nimetus, ManagerID")] Osakond osakond)
        {
            if (ModelState.IsValid)
            {
                db.Entry(osakond).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(osakond);
        }

        // GET: Osakond/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Osakond osakond = db.Osakond.Find(id);
            if (osakond == null)
            {
                return HttpNotFound();
            }
            return View(osakond);
        }

        // POST: Osakond/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Osakond osakond = db.Osakond.Find(id);

            foreach (var user in db.Kasutaja)
            {
                if (user.OsakondID == osakond.Id)
                {
                    user.OsakondID = null;   
                }
            }

            db.Osakond.Remove(osakond);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
