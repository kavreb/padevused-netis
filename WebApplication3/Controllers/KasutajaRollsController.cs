﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication3;

namespace WebApplication3
{
    partial class Kasutaja
    {
        // selle rea võiks iga dataklassi laienduse ja kontrolleri algusse lisada
        static InimesedPadevusedEntities db = new InimesedPadevusedEntities();

        // Tänu sellele funktsioonile saame emaili järgi kasutaja
        public static Kasutaja ByEmail(string email)
        {
            return db.Kasutaja.Where(x => x.Email == email).Take(1).SingleOrDefault();  //Kui meie kasutaja andmebaasis ei leita vastava emailiga tegelast, siis annab väärtuse null
        }

        public static Kasutaja ById(int? id)
        {
            return db.Kasutaja.Where(x => x.Id == id).Take(1).SingleOrDefault();
        }
    }
    partial class KasutajaRoll
    {
        // selle rea võiks iga dataklassi laienduse ja kontrolleri algusse lisada
        static InimesedPadevusedEntities db = new InimesedPadevusedEntities();

        // Tänu sellele funktsioonile saame Kasutaja tabeli Id järgi RollId KasutajaRoll tabelist
        public static KasutajaRoll ById(int id)
        {
            return db.KasutajaRoll.Where(x => x.KasutajaId == id).Take(1).SingleOrDefault();  //
        }
    }
    partial class Roll
    {
        // selle rea võiks iga dataklassi laienduse ja kontrolleri algusse lisada
        static InimesedPadevusedEntities db = new InimesedPadevusedEntities();

        // Tänu sellele funktisoonile saame KasutajaRoll tabeli RollId järgi Roll tabelist Id
        public static Roll ByRollId(int rollId)
        {
            return db.Roll.Where(x => x.Id == rollId).Take(1).SingleOrDefault();  //
        }
        
        //Meetod kontrollimaks, kas tegu on adminiga
        static bool OnAdmin = false;
        public static bool KasOnAdmin(int rollId)
        {
            if(rollId == 2)
            {return OnAdmin = true;}
            else
            {return OnAdmin = false;}
        }
        
        //Meetod kontrollimaks, kas tegu on employeega
        static bool OnEmployee = false;
        public static bool KasOnEmployee(int rollId)
        {
            if (rollId == 1)
            { return OnEmployee = true; }
            else
            { return OnEmployee = false; }
        }

        //Meetod kontrollimaks, kas tegu on manageriga
        static bool OnManager = false;
        public static bool KasOnManager(int rollId)
        {
            if (rollId == 3)
            { return OnManager = true; }
            else
            { return OnManager = false; }
        }

    }
   
}
namespace WebApplication3.Controllers
{

    public class KasutajaRollsController : Controller
    {
        private InimesedPadevusedEntities db = new InimesedPadevusedEntities();

        // GET: KasutajaRolls
        public ActionResult Index(string sortOrder)
        {
            var kasutajaRoll = db.KasutajaRoll.Include(k => k.Kasutaja).Include(k => k.Roll);
            
            switch (sortOrder)
            {
                case "nimi_desc":
                    ViewBag.Nimi = "nimi_desc";
                    return View(kasutajaRoll.ToList().OrderByDescending(x => x.Kasutaja.Nimi).ThenBy(s => s.Kasutaja.Id));
                        
                case "nimi_asc":
                    ViewBag.Nimi = "nimi_asc";
                    return View(kasutajaRoll.ToList().OrderBy(x => x.Kasutaja.Nimi).ThenBy(s => s.Kasutaja.Id));

                case "email_desc":
                    ViewBag.Email = "email_desc";
                    return View(kasutajaRoll.ToList().OrderByDescending(x => x.Kasutaja.Email));

                case "email_asc":
                    ViewBag.Email = "email_asc";
                    return View(kasutajaRoll.ToList().OrderBy(x => x.Kasutaja.Email));

                case "roll_desc":
                    ViewBag.Roll = "roll_desc";
                    return View(kasutajaRoll.ToList().OrderByDescending(x => x.Roll.Nimetus));

                case "roll_asc":
                    ViewBag.Roll = "roll_asc";
                    return View(kasutajaRoll.ToList().OrderBy(x => x.Roll.Nimetus));

                default:
                    ViewBag.Nimi = "nimi_asc";
                    return View(kasutajaRoll.ToList().OrderBy(x => x.Kasutaja.Nimi).ThenBy(s => s.Kasutaja.Id));
            }
        }

        // GET: KasutajaRolls/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaRoll kasutajaRoll = db.KasutajaRoll.Find(id);
            if (kasutajaRoll == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaRoll);
        }

        // GET: KasutajaRolls/Create
        public ActionResult Create()
        {
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email");
            ViewBag.RollId = new SelectList(db.Roll, "Id", "Nimetus");
            return View();
        }

        // POST: KasutajaRolls/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,KasutajaId,RollId")] KasutajaRoll kasutajaRoll)
        {
            if (ModelState.IsValid)
            {
                db.KasutajaRoll.Add(kasutajaRoll);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kasutajaRoll.KasutajaId);
            ViewBag.RollId = new SelectList(db.Roll, "Id", "Nimetus", kasutajaRoll.RollId);
            return View(kasutajaRoll);
        }

        // GET: KasutajaRolls/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaRoll kasutajaRoll = db.KasutajaRoll.Find(id);
            if (kasutajaRoll == null)
            {
                return HttpNotFound();
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kasutajaRoll.KasutajaId);
            ViewBag.RollId = new SelectList(db.Roll, "Id", "Nimetus", kasutajaRoll.RollId);
            return View(kasutajaRoll);
        }

        // POST: KasutajaRolls/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,KasutajaId,RollId")] KasutajaRoll kasutajaRoll)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kasutajaRoll).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KasutajaId = new SelectList(db.Kasutaja, "Id", "Email", kasutajaRoll.KasutajaId);
            ViewBag.RollId = new SelectList(db.Roll, "Id", "Nimetus", kasutajaRoll.RollId);
            return View(kasutajaRoll);
        }

        // GET: KasutajaRolls/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KasutajaRoll kasutajaRoll = db.KasutajaRoll.Find(id);
            if (kasutajaRoll == null)
            {
                return HttpNotFound();
            }
            return View(kasutajaRoll);
        }

        // POST: KasutajaRolls/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KasutajaRoll kasutajaRoll = db.KasutajaRoll.Find(id);
            db.KasutajaRoll.Remove(kasutajaRoll);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
