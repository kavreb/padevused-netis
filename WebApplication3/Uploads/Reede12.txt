Meetod:
meetod koosneb lausetest
lauseid on 4 liiki laused
mis defineerivad muutujad mida jargmises lauses hea kasutada.

string nimi;- defineerib.
Lokaalne muutuja on ainult {} sees

laused mitmesuguste meetodite valjakutseks
console.writeline ("..") - meetodi valjakutse. Teeb midagi. paikneb klassiss.
antakse parameetrite vaartused ette

nimi=Console.ReadLIne();  -tegemist on avaldisega. avaldises on tehem�rgid sees (naiteks =). 
			selles avaldises on funktsiooni valjakutse
			F-on on nagu meetod, aga tema kaist saadakse vastust
			Meetod ei anna vastust(void)
F-on on alati kindlat tuupi

4. Lauseste blokid (ta ise on ka lause)
koige lihtsam blokk
{
	// see on lihtsalt "sulle v�etud" port lauseid
	// selle blokis kirjedatud muutujad on naha ainult selle bloki sees
}

keerukamad blokid koosnevad PAISEST ja kehast
if(nimi == "Henn") -pais
{
	siin asub bloki keha
}

if lausele voib jargneda else blokk voi lause
{ -else blokk
	-kui koosneb uhest lausest siis voib loogilised sulud ara jatta
}

enamasti on bloki pais alguses ja siis on keha, on ainult uks lause kus blokil on saba
do
{
	nimi=console.ReadLine()
} while (nimi==Sarv)  - do blokil on olemas saba

me tunneme praegu veel selliseid blokke

while
for
foreach
else if on else blokk, mis sisaldab if lauset
switch

HOIATAN-blokke on veel
using, try catch, checked-uncheked, locked ....jne
  
blokkide moodi on ka
-class, void Meetodid, funktsiooonid, enumi....jne
-tunneme {} sulude jargi ja sissu


koik laused on kas blokid v�i lopevad ; kui bloki see on laused siis nemad lopevad;

CLASS
klassi nimi on uhtlasi andme tuup
on voimalik teha selle klassi muutuja

andme tuubid
int arv  - value tuubiga, saab anda lihtsalt vaartused
string nimi - ka value tuup, kuhu saab panna ka teksti sisse

//instantsi loomiseks( vaartuse eksemplaari loomseks on new korraldus, mis on tehe
mille vaartuseks on selle tuupi objekt).


Classi sissu kaib {} sulude vahele. Klass koosneb andmevaljadest (field) ja meetoditest 
v�i funktsioonidest.
public string Nimi // field v�i andmev�li on klassi sissene muutuja
	aga erinevalt lokaalselt (meetodi/funktsiooni siseselt) v�ib kasutada klassi muutuja ka 
	valjaspool klassi (ehk paistab kaugele) ja reeglina kirjutatakse suure t�hega

public int Vanus;
public void Vanane(int mitu) //tegemist klassi meetodiga , millega voimalik vanusele kui muutujale
		liita asju otse	
{
Vanus+=mitu;
}

funktsioon on meetod mis annab midagi tagasi

public override string ToString()-annab mingi stringi tuupi asja mida saab vala trukkida
{   
   return " mida tahad valja kirjutada "  
	return Nimi        
}

MILLAL STRINGI ETTE $ PANNNA
kolm varianti stringi tuuupi mida annavad sama varianti, et need saaks vorrelda
1. lithe  avaldis

2. keeruline avaldis
string.format ("{} on nii vana {}", Nimi, Vanus) on nii vana  - antakse parameeter mda panna {} plaseholderile asemele

3. interpoleeritud sting -algab dollariga ja sissaldab {}, ei ole string,vaid avaldis. Saame kasutada ainult vaartust
		selle sees tehakse ToString
$" {Nimi} on nii vana {Vanus}   "



f-nil peab olema return lause, mis on selle f-ni vastuse tuupi (kui meil naidiseks on string
siis vastub peab olma ka string)

public void Noorene (int mitu)
{
Vanus=Vanus < mitu ? 0 : Vanus- mitu;
// kun aei saa olla vahem kui 0 aastat, siis vaja lisa tingimust panna
}

Klassi sees ei pea alguses algv��rtust anmda, sest klassi siseltel muutujatel on olemas
vaikimisi vaartus. k�ikil klassi sissestel ajades ja on selle tuupi null.

Referentside algv��rtus on kandiline null.


Muutujad klassi sees
JUURDEP��SU m��ratus- kaib nii fiedide(muutujate) kui ka meetodide ja f-de kohta

private - asju naevada ainult teise meetodid ja avaldised  selles klassis

public -ajsu naevad k�ik selle klassi asjad. selle programmi asjad ja muud asjad 
	valjaspool meie programmi

INTERNAL - sisemised, neid naevad meie programmi asjad selles klassis kui ka teised
	klassid samas programmis

PROTECTED - kui meil on inimene klass ja keegi on sellest tueltanud opilase klassi
		ja teine on teinud veel Opetaja klassi. siis protected asju naevad
		tuletatud klassides olevad asjad
Inimene-> Opilane ja Inimne -> Opetaja
Private protected ja Internal protected (seda veel ei tea)

private string _Salajane="salas�na";
valjaspool selle klassi liigi ei saa, 
kuid saame teha meetodi mille kaudu saame sisse

- selle meetodi nimetakse Private valja exponeerimine

public string getSalajane() // getter
{
	return Salajane;
}

public void setSAlajane (string x) // setter
{
	//if (kontroll) -kontroll on labitud
	Salajane=x;
 }

Setteritel ja Getteritel on erinevad liigipasud
C# on getterite ja setterite asemel on property
get ja set ei pea olema sama juurde paasuga

public string Avalik
{
kosneeb kahest metodist (get, set) - on asendatud propertiga
	get {return Salajane; } on f-oon;  Juurde paasu modifikaator ei saa olla laiem kui algvaartus
	set {Salajane=value; }- on meetod; Set meetodiga saab aga kintsetada, naiteks privatiks teha
}
-seda saab luhemaks kirjutada (naide kuskil olemas)





private static int  uks=1;
public static int teine= uks*10;

Teatud tuup meetodeid kannab eraldi nimed. kui meil on vaja et klassi sees

???o n olemas veel meetodid ja 


ManguKooli naide:

Klassi nimed kaivad aintsuses!!!
Kolektsioonid mitmuses!!!!

enum on andmetuup mis koosneb nimelistest arvudest

enum Grade
kokku on 6 klassi

Alguses loome klass ja alles hiljem vaatame mis tal juurdep��s oleks

Vaike soovitus: elusatel nimel on elusnimed ja eluta on Tidel ja discription

@ algab eskeipimata string
\n
\t


kommit ja sync  -  vaatab ulevalt kas vahepeal on teine komment on tekkinud ja siis pushib komentaari
			siis kui tootab uksinda

et saada teed pull

kui hakkame koos tegema siis peab tegema sync ja teises masinas ka sync



